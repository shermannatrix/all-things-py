import sys

from PyQt6.QtWidgets import (
	QApplication, 
	QMainWindow,
	QDialog, 
    QMessageBox,
	QPushButton
)

# from custom_dialog import CustomDialog


class MainWindow(QMainWindow):

	def __init__(self):
		super().__init__()

		self.setWindowTitle("My App")

		button = QPushButton("Press me for a dialog!")
		button.clicked.connect(self.button_clicked)
		self.setCentralWidget(button)
	
	# In this code segment, we will explore using the critical()
	# static method.
	def button_clicked(self, s):
		
		button = QMessageBox.critical(
			self,
			"Oh dear!",
			"Something went very wrong.",
			buttons=QMessageBox.StandardButton.Discard | 
				QMessageBox.StandardButton.NoToAll |
				QMessageBox.StandardButton.Ignore,
			defaultButton=QMessageBox.StandardButton.Discard
		)

		if button == QMessageBox.StandardButton.Discard:
			print("Discard!")
		elif button == QMessageBox.StandardButton.NoToAll:
			print("No to all!")
		else:
			print("Ignore")

	# The code segment explored how to respond to which button
	# was pressed from the MessageBox that was created.
	# def button_clicked(self, s):
	# 	print("click", s)

	# 	dlg = QMessageBox(self)
	# 	dlg.setWindowTitle("I have a question!")
	# 	dlg.setText("This is a question dialog")
	# 	dlg.setStandardButtons(
	# 		QMessageBox.StandardButton.Yes | 
	# 		QMessageBox.StandardButton.No
	# 	)
	# 	dlg.setIcon(QMessageBox.Icon.Question)
	# 	button = dlg.exec()

    #     # Look up the button enum entry for the result.
	# 	button = QMessageBox.StandardButton(button)
		
	# 	if button == QMessageBox.StandardButton.Yes:
	# 		print("Yes!")
	# 	else:
	# 		print("No!")


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
