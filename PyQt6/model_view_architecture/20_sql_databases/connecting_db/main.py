import os
import sys

from PyQt6.QtCore import QSize, Qt
from PyQt6.QtSql import QSqlDatabase, QSqlTableModel
from PyQt6.QtWidgets import QApplication, QMainWindow, QTableView

basedir = os.path.dirname(__file__)

db = QSqlDatabase("QSQLITE")
db.setDatabaseName(os.path.join(basedir, "chinook.sqlite"))
db.open()


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.table = QTableView()

		self.model = QSqlTableModel(db=db)

		self.table.setModel(self.model)

		self.model.setTable("Track")

		# We can also set the Header titles here.
		# self.model.setHeaderData(1, Qt.Orientation.Horizontal, "Name")
		# self.model.setHeaderData(2, Qt.Orientation.Horizontal, "Album (ID)")
		# self.model.setHeaderData(3, Qt.Orientation.Horizontal, "Media Type (ID)")
		# self.model.setHeaderData(4, Qt.Orientation.Horizontal, "Genre (ID)")
		# self.model.setHeaderData(5, Qt.Orientation.Horizontal, "Composer")

		# defining those headers using a dictionary
		column_titles = {
			"Name": "Name",
			"AlbumId": "Album (ID)",
			"MediaTypeID": "Media Type (ID)",
			"GenreId": "Genre (ID)",
			"Composer": "Composer"
		}
		for index, title in column_titles.items():
			idx = self.model.fieldIndex(index)
			self.model.setHeaderData(idx, Qt.Orientation.Horizontal, title)

		# self.model.setSort(1, Qt.SortOrder.AscendingOrder)
		# When we want to use the column name instead.
		# idx = self.model.fieldIndex("Milliseconds")
		# self.model.setSort(idx, Qt.SortOrder.DescendingOrder)
		self.model.select()

		self.setMinimumSize(QSize(1024, 600))
		self.setCentralWidget(self.table)


app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec()
