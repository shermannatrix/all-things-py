import sys
import json

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt

from MainWindow import Ui_MainWindow

tick = QtGui.QImage('tick.png')

class TodoModel(QtCore.QAbstractListModel):
	def __init__(self, *args, todos=None, **kwargs):
		super(TodoModel, self).__init__(*args, **kwargs)
		self.todos = todos or []
	
	def data(self, index, role):
		if role == Qt.DisplayRole:
			# See below for the data structure.
			status, text = self.todos[index.row()]
			# Return the todo text only.
			return text
		
		if role == Qt.DecorationRole:
			status, _ = self.todos[index.row()]
			if status:
				return tick
	
	def rowCount(self, index):
		return len(self.todos)


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
	def __init__(self):
		super().__init__()
		self.setupUi(self)
		self.model = TodoModel()
		self.load()
		self.todoView.setModel(self.model)
		# Connect the button
		self.add_button.pressed.connect(self.add)
		self.delete_button.pressed.connect(self.delete)
		self.complete_button.pressed.connect(self.complete)
		
	
	def add(self):
		"""
		Add an item to our todo list, getting the text from the QLineEdit.todoEdit
		and then clearing it.
		"""
		text = self.todoEdit.text()
		text = text.strip() # Remove whitespace from the ends of the string.
		
		if text:    # Don't add empty strings.
			# Access the list via the model.
			self.model.todos.append((False, text))
			# Trigger refresh
			self.model.layoutChanged.emit()
			# Empty the input
			self.todoEdit.setText("")
			self.save()
	
	def delete(self):
		indexes = self.todoView.selectedIndexes()
		if indexes:
			# Indexes is a list of a single item in single-select mode.
			index = indexes[0]
			# Remove the item and refresh.
			del self.model.todos[index.row()]
			self.model.layoutChanged.emit()
			# Clear the selection (as it is no longer valid).
			self.todoView.clearSelection()
			self.save()
	
	def complete(self):
		indexes = self.todoView.selectedIndexes()
		if indexes:
			index = indexes[0]
			row = index.row()
			status, text = self.model.todos[row]
			self.model.todos[row] = (True, text)
			#.dataChanged takes top-left and bottom right, which are equal
			# for a single selection.
			self.model.dataChanged.emit(index, index)
			# Clear the selection (as it is no longer valid).
			self.todoView.clearSelection()
			self.save()
	
	def load(self):
		try:
			with open('data.json', 'r') as f:
				self.model.todos = json.load(f)
		except Exception:
			pass
	
	def save(self):
		with open('data.json', 'w') as f:
			data = json.dump(self.model.todos, f)


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
